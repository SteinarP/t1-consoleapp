const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });
  
//const {var, foo} = require("./utils");
const fs = require("fs");
const os = require("os");
const http = require("http");
const {PORT = 3000} = process.env;
//const { start } = require("repl");

console.log("Hello world!");
const highlight = str => "\033[34;1m"+str+"\033[0m";;


function readPackageJson(){
    console.log("Reading package.json...");
    fs.readFile(__dirname + "/package.json", "utf-8", (err, content) => {
        if (err){
            console.error("Something went wrong reading package.json!");
            throw err;
        }
        console.log(content);
    })
}

function displayOSInfo(){
    console.log("Getting OS info...");
    const toGB = mem => (mem / 1024 / 1024).toFixed(2) + " GB";
    console.log("-SYSTEM MEMORY:", toGB(os.totalmem()));
    console.log("-FREE MEMORY:", toGB(os.freemem()));
    console.log("-CPU CORES:", os.cpus().length);
    console.log("-ARCH:", os.arch());
    console.log("-RELEASE:", os.release());
    console.log("-HOST:", os.hostname());
    console.log("-USER:", os.userInfo().username);
}

function startHTTPServer(){
    console.log("Starting HTTP Server...");

    //Start
    const server = http.createServer((req,res) => {
        if (req.url === "/"){
            res.write("Hello World!");
            res.end();
        }
    
    
    });
    
    server.listen(PORT, ()=>{
        console.log("Started server. Try visiting "+highlight("http://localhost:"+PORT));
    })
    
    //End
}

const options = [   {func: showOptions, desc: "Show options"},
                    {func: readPackageJson, desc:"Read package.json"},
                    {func: displayOSInfo, desc: "Display OS Info"},
                    {func: startHTTPServer, desc: "Start HTTP Server"}
                ];
function showOptions(){
    options.forEach((o,i) => console.log(`${highlight(i)}. ${o.desc}`));
}

function takeCommand(){
    //\u001b
    readline.question("Enter command index:\n"+highlight("> "), ci => {
        let selected = options[ci];
        if (!selected){
            console.log(`[!] Invalid index: '${ci}'. \nType ${highlight("0")} to see options.`);
        } else {
            selected.func();
        }
        //readline.close();
        takeCommand(); 
      });  
}

showOptions();
takeCommand();

